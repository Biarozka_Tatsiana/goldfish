


import UIKit

enum Keys: String {
    case nameKey = "keyName"
    case fishKey = "keyFish"
    case firstGroupKey = "diverStarfishKey"
    case secondGroupKey = "netShark"
    case scoreKey = "scoreKey"
}

extension UIView {
    
    func cornerRadius () {
        self.layer.cornerRadius = 13
        self.layer.borderWidth = 1.5
    }
    
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

