

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func getString(from date: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let string = formatter.string(from: date)
        return string
    }
    
    func configure(object: Record) {
        
        scoreLabel.text = object.score
        dateLabel.text = getString(from: object.date)
        contentView.backgroundColor = .clear
    }
}
