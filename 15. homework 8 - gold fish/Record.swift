

import Foundation

class Record: Codable {
    
    var score: String
    var date: Date
    
    init (score: String, date: Date) {
        
        self.score = score
        self.date = date
    }
    
    private enum CodingKeys: String, CodingKey {
        
        case score
        case date
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        score = try container.decode(String.self, forKey: .score)
        date = try container.decode(Date.self, forKey: .date)
    }
    
    func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.score, forKey: .score)
        try container.encode(self.date, forKey: .date)
    }
}
