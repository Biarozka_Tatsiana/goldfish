

import UIKit
import RxSwift
import RxCocoa

class RecordsViewController: UIViewController {
    
    @IBOutlet weak var recordsView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var model = RecordModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recordsView.image = UIImage(named: "sea")
        recordsView.contentMode = .scaleToFill
        
        guard let text = UserDefaults.standard.value(forKey: Keys.nameKey.rawValue) as? String else {return}
        let info = "\(text)"
        let standardText = ", your score is"
        let fullText = info + standardText.localized
        userNameLabel.text = fullText
    }
    
    override func viewWillAppear (_ animated: Bool) {
        super.viewWillAppear(animated)
        
        model.score.bind(to: tableView.rx.items(cellIdentifier: "CustomTableViewCell", cellType: CustomTableViewCell.self)) { (row, element, cell) in
            cell.configure(object: element)
                 }
                 .disposed(by: disposeBag)
    }
}


