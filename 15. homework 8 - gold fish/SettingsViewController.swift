

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var goldFishButton: UIButton!
    @IBOutlet weak var pinkFishButton: UIButton!
    @IBOutlet weak var settingView: UIImageView!
    @IBOutlet weak var diverButton: UIButton!
    @IBOutlet weak var netButton: UIButton!
    @IBOutlet weak var seaStarButton: UIButton!
    @IBOutlet weak var sharkButton: UIButton!    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var fishSelectionLabel: UILabel!
    @IBOutlet weak var obstacleSelectionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let userNameText = "user name"
        userNameLabel.text = userNameText.localized
        let fishSelectionText = "select a fish"
        fishSelectionLabel.text = fishSelectionText.localized
        let obstacleSelectionText = "select an obstacle"
        obstacleSelectionLabel.text = obstacleSelectionText.localized
        let placeholderText = "enter your name"
        userNameTextField.placeholder = placeholderText.localized
        settingView.image = UIImage (named: "sea")
        settingView.contentMode = .scaleAspectFill
        showFishValue()
        showFirstGroupObstaclesValue ()
        showSecondGroupObstaclesValue()
        guard let userName = UserDefaults.standard.value(forKey: Keys.nameKey.rawValue) as? String else {return}
        userNameTextField.text = userName
    }
    
    func showFishValue () {
        
        guard let fishButton = UserDefaults.standard.value(Element.self, forKey: Keys.fishKey.rawValue) else { return }
        if  fishButton.imageName == "fish" && fishButton.buttonPressed {
            goldFishButton.cornerRadius()
            goldFishButton.layer.borderColor = UIColor.black.cgColor
        } else {
            pinkFishButton.cornerRadius()
            pinkFishButton.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    func showFirstGroupObstaclesValue () {
        
        guard let firstGroupObstaclesButton = UserDefaults.standard.value(Element.self, forKey: Keys.firstGroupKey.rawValue) else { return }
        if  firstGroupObstaclesButton.imageName == "diver" && firstGroupObstaclesButton.buttonPressed {
            diverButton.cornerRadius()
            diverButton.layer.borderColor = UIColor.black.cgColor
        } else {
            seaStarButton.cornerRadius()
            seaStarButton.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    func showSecondGroupObstaclesValue () {
        
        guard let secondGroupObstaclesButton = UserDefaults.standard.value(Element.self, forKey: Keys.secondGroupKey.rawValue) else { return }
        if  secondGroupObstaclesButton.imageName == "net" && secondGroupObstaclesButton.buttonPressed {
            netButton.cornerRadius()
            netButton.layer.borderColor = UIColor.black.cgColor
        } else {
            sharkButton.cornerRadius()
            sharkButton.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    func saveUserName () {
        
        guard let name = userNameTextField.text else {return}
        UserDefaults.standard.setValue(name, forKey: Keys.nameKey.rawValue)
    }
    
    @IBAction func goldFishButtonTapped(_ sender: Any) {
        
        goldFishButton.isSelected.toggle()
        let element = Element(imageName: "fish", buttonPressed: true)
        UserDefaults.standard.set(encodable: element, forKey: Keys.fishKey.rawValue)
        goldFishButton.cornerRadius()
        goldFishButton.layer.borderColor = UIColor.black.cgColor
        pinkFishButton.layer.borderColor = UIColor.clear.cgColor
    }
    
    @IBAction func pinkFishButtonPressed(_ sender: Any) {
        
        pinkFishButton.isSelected.toggle()
        let element = Element(imageName: "fish-102", buttonPressed: true)
        UserDefaults.standard.set(encodable: element, forKey: Keys.fishKey.rawValue)
        pinkFishButton.cornerRadius()
        pinkFishButton.layer.borderColor = UIColor.black.cgColor
        goldFishButton.layer.borderColor = UIColor.clear.cgColor
    }
    
    @IBAction func diverButtonPressed(_ sender: Any) {
        
        diverButton.isSelected.toggle()
        let element = Element(imageName: "diver", buttonPressed: true)
        UserDefaults.standard.set(encodable: element, forKey: Keys.firstGroupKey.rawValue)
        diverButton.cornerRadius()
        diverButton.layer.borderColor = UIColor.black.cgColor
        seaStarButton.layer.borderColor = UIColor.clear.cgColor
    }
    
    @IBAction func seastarButtonPressed(_ sender: Any) {
        
        seaStarButton.isSelected.toggle()
        let element = Element(imageName: "starfish", buttonPressed: true)
        UserDefaults.standard.set(encodable: element, forKey: Keys.firstGroupKey.rawValue)
        seaStarButton.cornerRadius()
        seaStarButton.layer.borderColor = UIColor.black.cgColor
        diverButton.layer.borderColor = UIColor.clear.cgColor
    }    
    
    @IBAction func netButtonPressed(_ sender: Any) {
        
        netButton.isSelected.toggle()
        let element = Element(imageName: "net", buttonPressed: true)
        UserDefaults.standard.set(encodable: element, forKey: Keys.secondGroupKey.rawValue)
        netButton.cornerRadius()
        netButton.layer.borderColor = UIColor.black.cgColor
        sharkButton.layer.borderColor = UIColor.clear.cgColor
    }
    
    @IBAction func sharkButtonPressed(_ sender: Any) {
        
        sharkButton.isSelected.toggle()
        let element = Element(imageName: "shark", buttonPressed: true)
        UserDefaults.standard.set(encodable: element, forKey: Keys.secondGroupKey.rawValue)
        sharkButton.cornerRadius()
        sharkButton.layer.borderColor = UIColor.black.cgColor
        netButton.layer.borderColor = UIColor.clear.cgColor
    }
}

extension SettingsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.saveUserName()
        return true
    }
}
