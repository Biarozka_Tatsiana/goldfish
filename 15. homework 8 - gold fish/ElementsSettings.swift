

import Foundation

class Element: Codable {
    
    var imageName: String
    var buttonPressed: Bool
    init (imageName: String, buttonPressed: Bool) {
        
        self.imageName = imageName
        self.buttonPressed = buttonPressed
    }
    private enum CodingKeys: String, CodingKey {
        
        case imageName
        case buttonPressed
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        imageName = try container.decode(String.self, forKey: .imageName)
        buttonPressed = try container.decode(Bool.self, forKey: .buttonPressed)
    }
    
    func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.imageName, forKey: .imageName)
        try container.encode(self.buttonPressed, forKey: .buttonPressed)
    }
}


