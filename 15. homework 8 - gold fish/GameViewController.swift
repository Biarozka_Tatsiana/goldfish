

import UIKit
import CoreMotion

class GameViewController: UIViewController {
    
    @IBOutlet weak var gameView: UIImageView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    
    let fishImage = UIImageView ()
    let step = 10
    let coralsSide = 60
    let animationCoralsIntervalLeft = 2.0
    let timerCoralsIntervalLeft = 2.5
    let animationCoralsIntervalRight = 2.3
    let animationMultiplicity = 3.0
    let timerCoralsIntervalRight = 2.8
    let interval = 4.0
    let diverWidth = 60
    let diverHeight = 60
    let netSide = 60
    let wormSide = 40
    let timerNetInterval = 6.0
    let animationNetInterval = 4.0
    let wormInterval = 6.0
    let animationWormInterval = 4.5
    let scoreInterval = 1.0
    var timeScore = 0
    var timerScore = Timer()
    var isEnded = false
    var corals: [UIImage?] = [UIImage(named: "coral7"), UIImage(named: "coral2"), UIImage(named: "coral4")]
    var coralsRight: [UIImage?] = [UIImage(named: "coral13"), UIImage(named: "coral5"), UIImage(named: "coral1r")]
    let coralViewLeft = UIImageView ()
    let coralViewRight = UIImageView ()
    var index = 0
    var myArray: [Record] = []
    var motionManager = CMMotionManager()
    let fontSize: CGFloat = 32
    let fontName = "Baskerville-Bold"
    let stepIndex = 1
    let startIndex = 0
    var fishImageWidth = 80
    var fishImageHeight = 80
    let fishImageIncreased: CGFloat = 20
    let delay = 0.0
    let multiplicity = 2
    let accelerometerInterval = 0.01
    let wormScore = 5
    let gyroInterval = 0.5
    let animationFishInterval = 1.0
    let gyroRate = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createTextLeftButton()
        createTextRightButton()
        countUserScore()
    }
    
    func countUserScore() {
        
        timerScore = Timer.scheduledTimer(withTimeInterval: scoreInterval, repeats: true) { (timerScore) in
            self.timeScore += 1
            self.timeLabel.text = "\(self.timeScore)"
        }
    }
    
    func recordUserScore () {
        let scoreModel = Record(score: String(timeScore), date: Date())
        if let score = UserDefaults.standard.value([Record].self, forKey: Keys.scoreKey.rawValue) {
            myArray = score
        }
        myArray.append(scoreModel)
        myArray.sort { (record1, record2) -> Bool in
            Int(record1.score) ?? 0 > Int(record2.score) ?? 0
        }
        UserDefaults.standard.set(encodable: myArray, forKey: Keys.scoreKey.rawValue)
    }
    
    func createTextLeftButton () {
        let textLeft = "left".localized
        let myAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: fontName, size: fontSize) as Any]
        let attrStringLeft = NSAttributedString(string: textLeft, attributes: myAttribute)
        leftButton.setAttributedTitle(attrStringLeft, for: .normal)
    }
    
    func createTextRightButton () {
        let textRight = "right".localized
        let myAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: fontName, size: fontSize) as Any]
        let attrStringRight = NSAttributedString(string: textRight, attributes: myAttribute)
        rightButton.setAttributedTitle(attrStringRight, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        isEnded = false
        gameView.image = UIImage (named: "sea")
        gameView.contentMode = .scaleAspectFill
        buttonsView.backgroundColor = .clear
        leftButton.cornerRadius()
        rightButton.cornerRadius()
        view.addSubview(buttonsView)
        createFishImage()
        createCoralsImagesLeft()
        createCoralsImagesRight()
        animateDiver()
        animateNet()
        animateWorm()
        moveFishImageThroughAccelerometer()
        shakeFish()
    }
    
    func moveFishImageThroughAccelerometer () {
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = accelerometerInterval
            motionManager.startAccelerometerUpdates(to: .main) { [weak self] (data: CMAccelerometerData?, error: Error?) in
                if let acceleration = data?.acceleration {
                    if acceleration.x < 0 {
                        self?.moveFishImageLeft()
                    }
                    if acceleration.x > 0 {
                        self?.moveFishImageRight()
                    }
                }
            }
        }
    }
    
    func shakeFish() {
        
        if motionManager.isDeviceMotionAvailable {
            motionManager.gyroUpdateInterval = gyroInterval
            motionManager.startGyroUpdates(to: .main) { (data, error) in
                if let rate = data?.rotationRate {
                    if rate.x > self.gyroRate || rate.y > self.gyroRate || rate.y > self.gyroRate {
                        self.animateFish()
                    }
                }
            }
        }
    }
    
    func animateFish () {
        
        UIView.animate(withDuration: animationFishInterval, delay: delay, options: .curveLinear) {
            
            self.fishImage.frame.size.width += self.fishImageIncreased
            self.fishImage.frame.size.height += self.fishImageIncreased
            
        } completion: { (_) in
            
            self.fishImage.frame.size.width -= self.fishImageIncreased
            self.fishImage.frame.size.height -= self.fishImageIncreased
        }
    }
    
    func createCoralsImagesLeft ()  {
        
        _ = Timer.scheduledTimer(withTimeInterval: timerCoralsIntervalLeft, repeats: true) { [self] (timer) in
            
            self.coralViewLeft.frame = CGRect(x: Int(self.view.frame.maxX) - self.coralsSide, y:  -self.coralsSide, width: self.coralsSide, height: self.coralsSide)
            
            self.coralViewLeft.image = self.corals [self.index]
            self.view.addSubview(self.coralViewLeft)
            
            UIView.animate(withDuration: self.animationCoralsIntervalLeft, delay: delay, options: .curveLinear) {
                self.coralViewLeft.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.coralsSide)
            }
            completion: { (_) in
                
                if let coralViewLeftFrame = self.coralViewLeft.layer.presentation()?.frame, self.fishImage.frame.intersects (coralViewLeftFrame)  {
                    if !self.isEnded {
                        self.isEnded = true
                        self.navigationController?.popToRootViewController(animated: true)
                        self.timerScore.invalidate()
                        self.recordUserScore ()
                        timer.invalidate()
                    }
                } else {
                    UIView.animate(withDuration: self.animationCoralsIntervalLeft / animationMultiplicity, delay: delay, options: .curveLinear) {
                        self.coralViewLeft.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.coralsSide)
                    } completion: { (_) in
                        self.coralViewLeft.removeFromSuperview()
                    }
                }
            }
            if self.index < self.corals.count - self.stepIndex {
                self.index += self.stepIndex
                
            } else {
                self.index = self.startIndex
            }
        }
        view.layer.presentation()
    }
    
    func createCoralsImagesRight ()  {
        
        let timer = Timer.scheduledTimer(withTimeInterval: timerCoralsIntervalRight, repeats: true) { (timer) in
            
            self.coralViewRight.frame = CGRect(x: Int(self.view.frame.minX), y:  -self.coralsSide, width: self.coralsSide, height: self.coralsSide)
            
            self.coralViewRight.image = self.coralsRight [self.index]
            self.view.addSubview(self.coralViewRight)
            
            UIView.animate(withDuration: self.animationCoralsIntervalRight, delay: self.delay, options: .curveLinear) {
                self.coralViewRight.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.coralsSide)
            }
            completion: { (_) in
                
                if let coralViewRightFrame = self.coralViewRight.layer.presentation()?.frame, self.fishImage.frame.intersects (coralViewRightFrame) {
                    if !self.isEnded {
                        self.isEnded = true
                        self.timerScore.invalidate()
                        self.recordUserScore ()
                        timer.invalidate()
                    }
                    self.navigationController?.popToRootViewController(animated: true)
                } else {
                    UIView.animate(withDuration: self.animationCoralsIntervalRight / self.animationMultiplicity, delay: self.delay, options: .curveLinear) {
                        self.coralViewRight.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.coralsSide)
                    } completion: { (_) in
                        self.coralViewRight.removeFromSuperview()
                    }
                }
            }
            if self.index < self.coralsRight.count - self.stepIndex {
                self.index += self.stepIndex
            } else {
                self.index = self.startIndex
            }
        }
        timer.fire()
        view.layer.presentation()
    }
    
    func createWorm() -> UIImageView {
        
        let wormView = UIImageView()
        let image = UIImage(named: "worm")
        wormView.image = image
        let wormX = CGFloat.random(in: self.view.frame.minX + CGFloat(coralsSide) ... self.view.frame.maxX - CGFloat(coralsSide) - CGFloat(wormSide))
        wormView.frame = CGRect(x: Int(wormX), y: Int(self.view.frame.origin.y) - wormSide, width: wormSide, height: wormSide)
        wormView.contentMode = .scaleToFill
        self.view.addSubview(wormView)
        return wormView
    }
    
    func animateWorm () {
        
        _ = Timer.scheduledTimer(withTimeInterval: wormInterval, repeats: true) { (timer) in
            let worm = self.createWorm()
            UIView.animate(withDuration: self.animationWormInterval, delay: self.delay, options: .curveLinear) {
                worm.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.wormSide)
            } completion: { (_) in
                if let wormViewFrame = worm.layer.presentation()?.frame, self.fishImage.frame.intersects(wormViewFrame) {
                    self.timeScore += self.wormScore
                    worm.removeFromSuperview()
                } else {
                    UIView.animate(withDuration: self.animationWormInterval / self.animationMultiplicity, delay: self.delay, options: .curveLinear) {
                        worm.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.wormSide)
                    } completion: { (_) in
                        worm.removeFromSuperview()
                    }
                }
            }
            
        }
    }
    
    func createDiver() -> UIImageView? {
        
        let diverView  = UIImageView()
        guard let diver = UserDefaults.standard.value(Element.self, forKey: Keys.firstGroupKey.rawValue) else { return nil}
        let image = UIImage(named: diver.imageName)
        diverView.image = image
        let diverX = CGFloat.random(in: self.view.frame.minX + CGFloat(coralsSide) ... self.view.frame.maxX - CGFloat(coralsSide) - CGFloat(diverWidth))
        diverView.frame = CGRect(x: Int(diverX), y: Int(self.view.frame.origin.y) - diverHeight , width: diverWidth, height: diverHeight)
        diverView.contentMode = .scaleToFill
        self.view.addSubview(diverView)
        return diverView
    }
    
    func animateDiver () {
        
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (timer) in
            guard let diver = self.createDiver() else {return}
            UIView.animate(withDuration: self.interval, delay: self.delay, options: .curveLinear) {
                diver.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.diverHeight)
            } completion: { (_) in
                if let diverViewFrame = diver.layer.presentation()?.frame, self.fishImage.frame.intersects (diverViewFrame) {
                    if !self.isEnded {
                        self.isEnded = true
                        self.navigationController?.popToRootViewController(animated: true)
                        self.timerScore.invalidate()
                        self.recordUserScore()
                        timer.invalidate()
                    }
                } else {
                    UIView.animate(withDuration: self.interval / self.animationMultiplicity, delay: self.delay, options: .curveLinear) {
                        diver.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.diverHeight)
                    } completion: { (_) in
                        diver.removeFromSuperview()
                    }
                }
            }
        }
        timer.fire()
        view.layer.presentation()
    }
    
    func createNet () -> UIImageView? {
        
        let netView  = UIImageView()
        guard let net = UserDefaults.standard.value(Element.self, forKey: Keys.secondGroupKey.rawValue) else { return nil }
        let image = UIImage(named: net.imageName)
        netView.image = image
        let netX = CGFloat.random(in: self.view.frame.minX + CGFloat(coralsSide) ... self.view.frame.maxX - CGFloat(coralsSide) - CGFloat(netSide))
        netView.frame = CGRect(x: Int(netX), y: Int(self.view.frame.origin.y) - netSide , width: netSide, height: netSide)
        netView.contentMode = .scaleToFill
        self.view.addSubview(netView)
        return netView
    }
    
    func animateNet () {
        
        _ = Timer.scheduledTimer(withTimeInterval: timerNetInterval, repeats: true) { (timer) in
            guard let net = self.createNet() else {return}
            UIView.animate(withDuration: self.animationNetInterval, delay: self.delay, options: .curveLinear) {
                net.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.netSide)
            } completion: { (_) in
                if let netViewFrame = net.layer.presentation()?.frame, self.fishImage.frame.intersects (netViewFrame)  {
                    if !self.isEnded {
                        self.isEnded = true
                        self.navigationController?.popToRootViewController(animated: true)
                        self.timerScore.invalidate()
                        self.recordUserScore ()
                        timer.invalidate()
                    }
                } else {
                    UIView.animate(withDuration: self.animationNetInterval / self.animationMultiplicity, delay: self.delay, options: .curveLinear) {
                        net.frame.origin.y = self.view.frame.height - self.buttonsView.frame.height - CGFloat(self.netSide)
                    } completion: { (_) in
                        net.removeFromSuperview()
                    }
                }
            }
        }
        view.layer.presentation()
    }
    
    func createFishImage () {
        
        guard let fish = UserDefaults.standard.value(Element.self, forKey: Keys.fishKey.rawValue) else { return }
        let image = UIImage(named: fish.imageName)
        self.fishImage.image = image
        fishImage.frame = CGRect(x: Int(self.view.frame.width) / multiplicity - fishImageWidth / multiplicity, y: Int(self.view.frame.height - self.buttonsView.frame.height - CGFloat(fishImageHeight)) - multiplicity * step, width: fishImageWidth, height: fishImageHeight)
        fishImage.contentMode = .scaleToFill
        view.addSubview(fishImage)
    }
    
    func moveFishImageLeft () {
        
        if fishImage.frame.origin.x - CGFloat(step) > view.frame.origin.x {
            fishImage.frame.origin.x -= CGFloat(step)
        }
    }
    
    func moveFishImageRight () {
        
        if fishImage.frame.origin.x + CGFloat(step) + fishImage.frame.width < view.frame.maxX {
            fishImage.frame.origin.x += CGFloat(step)
        }
    }
    
    @IBAction func leftButtonPressed(_ sender: Any) {
        
        moveFishImageLeft()
    }
    
    @IBAction func rightButtonPressed(_ sender: Any) {
        
        moveFishImageRight()
    }
}

