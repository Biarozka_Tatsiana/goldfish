
import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var startView: UIImageView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var recordsButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    let fontSize: CGFloat = 37
    let fontName = "Baskerville-Bold"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startView.image = UIImage(named: "startView")
        startView.contentMode = .scaleAspectFill
        buttonsView.backgroundColor = .clear
        startButton.cornerRadius()
        recordsButton.cornerRadius()
        settingsButton.cornerRadius()
        view.addSubview(buttonsView)
        createTextStartButton()
        createTextRecordsButton()
        createTextSettingsButton()
    }
    
    
    func createTextStartButton () {
        let text = "start".localized
        let myAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: fontName, size: fontSize) as Any]
        let attrString = NSAttributedString(string: text, attributes: myAttribute)
        startButton.setAttributedTitle(attrString, for: .normal)
    }
    
    func createTextRecordsButton () {
        let text = "records".localized
        let myAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: fontName, size: fontSize) as Any]
        let attrString = NSAttributedString(string: text, attributes: myAttribute)
        recordsButton.setAttributedTitle(attrString, for: .normal)
    }
    
    func createTextSettingsButton () {
        let text = "settings".localized
        let myAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: fontName, size: fontSize) as Any]
        let attrString = NSAttributedString(string: text, attributes: myAttribute)
        settingsButton.setAttributedTitle(attrString, for: .normal)
    }
    
    @IBAction func gameButtonPressed(_ sender: Any) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as? GameViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func settingsButtonPressed(_ sender: Any) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func recordsButtonPressed(_ sender: Any) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as? RecordsViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

